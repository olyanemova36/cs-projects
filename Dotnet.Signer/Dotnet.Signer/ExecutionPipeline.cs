using System;
using System.Threading.Channels;
using System.Threading.Tasks;
using static Dotnet.Signer.Common;

namespace Dotnet.Signer
{
    public class ExecutionPipeline
    {
        public ExecutionPipeline()
        {
            
            
        }

        public async Task Execute(params Job[] jobs)
        {
            // TODO: implement
        }
        
        public async Task ExecuteBad(params Job[] jobs)
        {
            // Эта реализация работает крайне медленно и неправильно. Зато даёт корректный результат
            // вычисления. Можете использовать её для какой-нибудь отладки.
            var c1 = Channel.CreateBounded<object>(MaxInputDataLength);
            var c2 = Channel.CreateBounded<object>(MaxInputDataLength);
            var c3 = Channel.CreateBounded<object>(MaxInputDataLength);
            var c4 = Channel.CreateBounded<object>(MaxInputDataLength);
            var c5 = Channel.CreateBounded<object>(MaxInputDataLength);
            var c6 = Channel.CreateBounded<object>(MaxInputDataLength);

            c1.Writer.Complete();
            await jobs[0].Invoke(c1.Reader, c2.Writer);
            c2.Writer.Complete();
            Console.WriteLine("job 0 complete");
            await jobs[1].Invoke(c2.Reader, c3.Writer);
            c3.Writer.Complete();
            Console.WriteLine("job 1 complete");
            await jobs[2].Invoke(c3.Reader, c4.Writer);
            c4.Writer.Complete();
            Console.WriteLine("job 2 complete");
            await jobs[3].Invoke(c4.Reader, c5.Writer);
            c5.Writer.Complete();
            Console.WriteLine("job 3 complete");
            await jobs[4].Invoke(c5.Reader, c6.Writer);
            c6.Writer.Complete();
            Console.WriteLine("job 4 complete");
        }
    }
}