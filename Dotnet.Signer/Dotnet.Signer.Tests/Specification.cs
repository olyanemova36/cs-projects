using System;
using Xunit.Abstractions;

namespace Dotnet.Signer.Tests
{
    public class Specification
    {
        protected ITestOutputHelper Logger { get; }
        
        public Specification(ITestOutputHelper testOutputHelper)
        {
            Console.SetOut(new ConsoleOutputConverter(testOutputHelper));
            this.Logger = testOutputHelper;
        }
    }
}