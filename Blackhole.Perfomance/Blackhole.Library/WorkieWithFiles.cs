﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("Blackhole.Perfomance")]
namespace Blackhole.Library
{
    public class WorkieWithFiles
    {
        private string fileName = " ";
        private List<string> source = new List<string>();

        public string getFileName()
        {
            return fileName;
        }
        
        public void ReadFile(string filename)
        {
            /*int count = 0;
            foreach (string line in System.IO.File.ReadLines(@filename))
            {
                this.source.Add(line);
                Array words = line.Split(' ');
                count += words.Length;
            }*/
            var fileStream = File.OpenRead(filename);
            var read = 0;
            int wordsCounter = 0;
            var buffer = new byte[4096];
            var length = fileStream.Length;
            while (read != length)
            {
                read += fileStream.Read(buffer);
                string word = string.Empty;
                foreach (var c in Encoding.Default.GetChars(buffer))
                {
                    word += c;
                    if (c == '\n')
                    {
                        continue;
                    }
                    if (c == ' ')
                    {
                        this.source.Add(word);
                        word = word.Remove(0, word.Length);
                        wordsCounter++;
                    }
                }
            }

        }
        
        public int CountWords()
        {
            int count = 0;
            /*foreach (string line in this.source)
            {
                Array words = line.Split(' ');
                count += words.Length;
            }*/
            count = this.source.Count;
            return count;
        }

    }
}