﻿using System;
using System.Diagnostics;
using System.IO;
using Prometheus;
using Prometheus.DotNetRuntime;
using System.Text;


/*namespace Blackhole.Graphics
{
    internal class Program
    {
        
        private static readonly Gauge PersentsOfReadTheFile = Metrics.CreateGauge(
            "dotnet_percents_of_file",
            "Depth of the stack of braces"
        );
        private static readonly Gauge Time = Metrics.CreateGauge(
            "dotnet_time_of_row",
            "Depth of the stack of braces"
        );
        
        private static readonly Counter RowCounter = Metrics.CreateCounter(
            "dotnet_row_counter_in_file",
            "Count of read strokes"
        );
        private static readonly Histogram TimeOfReading = Metrics.CreateHistogram(
            "dotnet_histogram_of_time",
            "Count time of one stroke reading"
        );

        private static void Main(string[] args)
        {
            using var collector = DotNetRuntimeStatsBuilder
                .Default()
                .StartCollecting();
            var metricServer = new MetricServer(port: 6559);
            metricServer.Start();
            var filename = "/etc/files/50Mb.txt";
            //var filename = "/Users/olganemova/RiderProjects/Blackhole.Perfomance/Blackhole.Graphics/5Mb.txt";
            double totalCount = System.IO.File.ReadAllLines(filename).Length; 
            while (true)
            {
                    using var fileStream = File.OpenRead(filename);
                    var read = 0;
                    long rowCounter = 0;
                    long wordsCounter = 0;
                    var buffer = new byte[4096];
                    var length = fileStream.Length;
                    Stopwatch startTime =  new Stopwatch();
                    while (read != length)
                    {
                        read += fileStream.Read(buffer);
                        foreach (var c in Encoding.Default.GetChars(buffer))
                        {
                            if (c == '\n')
                            {
                                rowCounter++;
                                startTime.Stop();
                                RowCounter.Inc(1);
                                Time.Set(startTime.Elapsed.Milliseconds);
                                TimeOfReading.Observe(startTime.Elapsed.Milliseconds);
                                PersentsOfReadTheFile.Set((rowCounter / totalCount) * 100);
                                startTime.Start();
                                continue;
                            }
                            if (c == ' ')
                            {
                                wordsCounter++;
                            }
                        }
                    }
                    Console.WriteLine(rowCounter);
            
            }

        }
    }
}*/


namespace Blackhole.Graphics
{
    internal class Program
    {
        
        private static readonly Gauge PersentsOfReadTheFile = Metrics.CreateGauge(
            "dotnet_percents_of_file",
            "Depth of the stack of braces"
        );
        private static readonly Gauge Time = Metrics.CreateGauge(
            "dotnet_time_of_row",
            "Depth of the stack of braces"
        );
        
        private static readonly Counter RowCounter = Metrics.CreateCounter(
            "dotnet_row_counter_in_file",
            "Count of read strokes"
        );
        private static readonly Histogram TimeOfReading = Metrics.CreateHistogram(
            "dotnet_histogram_of_time",
            "Count time of one stroke reading"
        );

        private static void Main(string[] args)
        {
            using var collector = DotNetRuntimeStatsBuilder
                .Default()
                .StartCollecting();
            var metricServer = new MetricServer(port: 6559);
            metricServer.Start();
            var filename = "/etc/files/50Mb.txt";
            double count = System.IO.File.ReadAllLines(filename).Length;
            int current_count;
            while (true)
            {
                current_count = 0;
                Stopwatch startTime =  new Stopwatch();
                foreach (string line in System.IO.File.ReadLines(filename))
                {
                    startTime.Start();
                    PersentsOfReadTheFile.Set((current_count / count) * 100);
                    var words = line.Split(' ');
                    var counter = words.Length;
                    current_count++;
                    RowCounter.Inc(1);
                    startTime.Stop();
                    Time.Set(startTime.Elapsed.Milliseconds);
                    TimeOfReading.Observe(startTime.Elapsed.Milliseconds);
                }
                
            }

        }
    }
}
