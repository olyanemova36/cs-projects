# Отчет об исследовании производительности приложения № 2

Проводился повторный анализ кода с изменённой реализацией метода чтения строк из файла.

Повторно проводимые тесты:

`50Mb.txt`
1. Micro benchmarking 
2. Macro benchmarking 

`1GB.txt`
1. Sampling
2. Cбор метрик производительности с помощью Prometheus

## Анализ производительности с помощью Benchmarking

Проводился Benchmarking (Micro) заново двух функций `ReadFile(string filename)` и `CountWords()` для класса
`WorkieWithFiles`.

Посмотрим на повторные результаты прогона Бенчамркинга.

![](img/Microbenchmarking_2.png)
*Рисунок 1. Микробенчмаркинг. Вторая реализация*

![](img/Macrobenchmarking_2.png)
*Рисунок 2. Макробенчмаркинг. Вторая реализация*

![](img/Macrobenchmarking_3.png)
*Рисунок 3. Макробенчмаркинг. Третья реализация*

По показателям, сразу можно сказать, что вес аллоцированной памяти уменьшился почти в 6 (5,76) раз и прогон метода чтения файла стал в несколько раз быстрее (оцениваем по медиане среднее время выполнения, должно было оптимизироваться в около 13 раз).
Повторная реализация дала выигрыш в памяти и быстроте прогона метода. 

Я провела третий Макробенчмаркинг, но теперь намного возрасло количество памяти, но средние величины уменьшились в разы, что может подсказывать об увеличении быстроты программы.

Посмотрим на показатели сбора профиля.


## Sampling 

Проводился Sampling двух функций `ReadFile(string filename)` и `CountWords()` для класса
`WorkieWithFiles` над файлом размером `1GB.txt`.
Снепшоты показывают, что теперь вклад функции Чтения файла намного больше, но это правильный результат, ведь теперь при. подсчете количества строк используется не цикл, а всего лишь команда получить длину массива. И общий клад хоть и стал больше в процентах, но по величине он уменьшился, что является хорошим знаком.

![](img/Sampling_2.png)
*Рисунок 3. Семплинг (Сбор профиля). Вторая реализация*

## Prometheus + Graphana

Проводился сбор метрик с анализа функции `ReadFile(string filename)`, её аналога, 
реализованного в `Main()`.

![](img/Graphana_2.png)
*Рисунок 4. Prometheus. Сбор метрик. Вторая реализация*

__Гистограма__

По гистрограмме видно, что распределение 
имеет вид *Равномерного* опять, это является хорошим знаком, но было бы отлично, если бы оно имело вид експотенциального. Почему? 
Потому что, тогда мы бы видели, что за ближайшие 5 минут мы обрабатываем больше строк, и количество быстро обрабатываемых строк увеличилось бы. Как такое можно реализовать? Думаю, хорошим способом явлется предварительна сортировка, но особенная, чтобы обрабатывать сначала более короткие строки, а затем более тяжеловесные (Если мое предположение неверно, пожалуйста, поправте).

Но видим, что по гистограмме, диапазон по оси Х увеличился, что является знаком того, что некоторые строки стали обрабатываться дольше. Проверим другие статистики.

__Memory Usage__

Теперь у графика нет скачков, это хорошо! Нет больших всплесков памяти, это значит, что метод работает более менее равномерно и не занимает в какие-то моменты очень много памяти, что делает его реализацию более стабильной.

__Percentage file__

Это наш флаг. Флаг сработал, оказывается мы замедлили программу, но улучшили использование памяти, что тоже явдяется хорошим знаком.

__Почему наш метод выиграл в памяти??__

Все очень просто. Поэтому произвольные массивы намного быстрее.

И как я помню, в зависимости от аллокатора, для списка выделяется сразу мощный кусок памяти, и потом он последовательно заполняет его, и если он совсем заполняется, то аллокатор копирует существующий новый список и увеличивает концы памяти в 2 стороны и затем перносит новый список туда. Что выглядит очень неэкономично по памяти.
Окружения с умным управлением памятью и сборщиками мусора, возможно, будут хранить узлы связного списка более последовательно, но они не могут гарантировать это. Использование сырого массива обычно требует более сложного кода, особенно когда нужно вставлять или добавлять новые элементы, так как придется обрабатывать рост массива, перемещение элементов, и так далее, но по весу он будет регулироваться только нами, а в списке совсем наоборот, там есть много памяти, где элементов много и они расположены хаотично, и выделяется много памяти для хранения дополнительно информации. 
(Поправте, если не так) 
И плюс массив побеждает потому, что числа, по которым происходит проход, находятся в памяти последовательно.

