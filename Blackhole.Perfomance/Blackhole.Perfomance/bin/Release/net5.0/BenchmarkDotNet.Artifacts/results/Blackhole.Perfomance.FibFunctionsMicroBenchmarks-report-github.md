``` ini

BenchmarkDotNet=v0.13.1, OS=macOS Monterey 12.0.1 (21A559) [Darwin 21.1.0]
Apple M1 2.40GHz, 1 CPU, 8 logical and 8 physical cores
.NET SDK=5.0.400
  [Host]     : .NET 5.0.9 (5.0.921.35908), X64 RyuJIT
  Job-BUEBLK : .NET 5.0.9 (5.0.921.35908), X64 RyuJIT

IterationCount=10  LaunchCount=1  RunStrategy=Throughput  
WarmupCount=10  

```
|            Method | Number |         Mean |      Error |    StdDev | Ratio | RatioSD |  Gen 0 | Allocated |
|------------------ |------- |-------------:|-----------:|----------:|------:|--------:|-------:|----------:|
|     **BenchFibNaive** |      **5** |     **89.60 ns** |   **0.322 ns** |  **0.213 ns** |  **1.00** |    **0.00** |      **-** |         **-** |
|  BenchFibMemoized |      5 |    260.21 ns |   0.246 ns |  0.163 ns |  2.90 |    0.01 | 0.2599 |     544 B |
| BenchFibOptimized |      5 |     79.65 ns |   3.422 ns |  2.264 ns |  0.89 |    0.03 | 0.0153 |      32 B |
|                   |        |              |            |           |       |         |        |           |
|     **BenchFibNaive** |     **10** |  **1,168.09 ns** |  **22.961 ns** | **13.664 ns** |  **1.00** |    **0.00** |      **-** |         **-** |
|  BenchFibMemoized |     10 |    584.19 ns |  24.431 ns | 14.539 ns |  0.50 |    0.02 | 0.5770 |   1,208 B |
| BenchFibOptimized |     10 |    152.41 ns |   1.813 ns |  1.079 ns |  0.13 |    0.00 | 0.0153 |      32 B |
|                   |        |              |            |           |       |         |        |           |
|     **BenchFibNaive** |     **15** | **12,995.93 ns** | **102.977 ns** | **61.280 ns** | **1.000** |    **0.00** |      **-** |         **-** |
|  BenchFibMemoized |     15 |    746.02 ns |   9.065 ns |  5.996 ns | 0.057 |    0.00 | 0.5770 |   1,208 B |
| BenchFibOptimized |     15 |    127.53 ns |   1.284 ns |  0.850 ns | 0.010 |    0.00 | 0.0153 |      32 B |
