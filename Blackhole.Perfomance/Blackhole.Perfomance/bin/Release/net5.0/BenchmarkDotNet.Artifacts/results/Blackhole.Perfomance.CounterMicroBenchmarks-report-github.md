``` ini

BenchmarkDotNet=v0.13.1, OS=macOS Monterey 12.0.1 (21A559) [Darwin 21.1.0]
Apple M1 2.40GHz, 1 CPU, 8 logical and 8 physical cores
.NET SDK=5.0.400
  [Host]     : .NET 5.0.9 (5.0.921.35908), X64 RyuJIT
  Job-HQRYJL : .NET 5.0.9 (5.0.921.35908), X64 RyuJIT

IterationCount=10  LaunchCount=1  RunStrategy=Throughput  
WarmupCount=10  

```
|          Method |             filename |                  Mean |               Error |             StdDev | Ratio |       Gen 0 |       Gen 1 |       Allocated |
|---------------- |--------------------- |----------------------:|--------------------:|-------------------:|------:|------------:|------------:|----------------:|
|   BanchRaedFile | ../..(...)b.txt [29] | 2,532,923,907.4444 ns | 106,280,287.0598 ns | 63,245,694.0715 ns |  1.00 | 832000.0000 | 243000.0000 | 2,997,959,024 B |
|                 |                      |                       |                     |                    |       |             |             |                 |
| BanchCountWords | ../..(...)b.txt [29] |             0.0069 ns |           0.0011 ns |          0.0007 ns |  1.00 |           - |           - |               - |
