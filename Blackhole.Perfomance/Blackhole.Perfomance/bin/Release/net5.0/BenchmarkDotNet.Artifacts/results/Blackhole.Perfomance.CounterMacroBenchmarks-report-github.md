``` ini

BenchmarkDotNet=v0.13.1, OS=macOS Monterey 12.0.1 (21A559) [Darwin 21.1.0]
Apple M1 2.40GHz, 1 CPU, 8 logical and 8 physical cores
.NET SDK=5.0.400
  [Host]     : .NET 5.0.9 (5.0.921.35908), X64 RyuJIT
  Job-IVBGFD : .NET 5.0.9 (5.0.921.35908), X64 RyuJIT

RunStrategy=Monitoring  

```
|       Method |    Mean |   Error |  StdDev |       Gen 0 |       Gen 1 | Allocated |
|------------- |--------:|--------:|--------:|------------:|------------:|----------:|
| BenchCounter | 2.985 s | 2.037 s | 1.348 s | 824000.0000 | 228000.0000 |      3 GB |
