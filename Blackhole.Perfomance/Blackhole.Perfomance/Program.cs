using System;
using BenchmarkDotNet.Running;
using Blackhole.Library;

namespace Blackhole.Perfomance
{

    internal class Program
    {
        private static void Main(string[] args)
        {
            _ = BenchmarkRunner.Run<CounterMicroBenchmarks>();
        }
    }
}