﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using Blackhole.Library;
namespace Blackhole.Perfomance
{
    [MemoryDiagnoser()]
    [SimpleJob(
        RunStrategy.Throughput,
        launchCount:1,
        warmupCount:10,
        targetCount:10,
        baseline:true
    )]
    public class CounterMicroBenchmarks
    {
        
        [Params("../../../../../../../50Mb.txt")] public string filename;
        WorkieWithFiles CoolFile = new WorkieWithFiles();
        
        [Benchmark]
        public void BanchRaedFile()
        {
            CoolFile.ReadFile(filename);
        }
        [Benchmark]
        public void BanchCountWords()
        {
            CoolFile.CountWords();
        }
    }
}