using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using Blackhole.Library;
namespace Blackhole.Perfomance
{
    [MemoryDiagnoser]
    [SimpleJob(
        RunStrategy.Monitoring
    )]
    public class CounterMacroBenchmarks
    {
       
        private string filename = "../../../../../../../50Mb.txt";
        private WorkieWithFiles coolFile;

        [GlobalSetup]
        public void GlobalSetup()
        {
            this.coolFile = new WorkieWithFiles();
        }
        
        [Benchmark]
        public void BenchCounter() { this.coolFile.ReadFile(this.filename);}

    }
}